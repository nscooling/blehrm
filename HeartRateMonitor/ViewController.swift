//
//  ViewController.swift
//  HeartRateMonitor
//
//  Created by Niall Cooling on 22/10/2014.
//  The MIT License (MIT)
//
//Copyright (c) 2014 Niall Cooling
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all
//copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//SOFTWARE.
//

import UIKit
import CoreBluetooth
import QuartzCore


class ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    let HRM_DEVICE_INFO_SERVICE_UUID               = CBUUID(string:"180A")
    let HRM_HEART_RATE_SERVICE_UUID                = CBUUID(string:"180D")
    let HRM_BATTERY_LEVEL_CHARACTERISTIC_UUID      = CBUUID(string:"180F")
    
    let HRM_MEASUREMENT_CHARACTERISTIC_UUID        = CBUUID(string:"2A37")
    let HRM_BODY_LOCATION_CHARACTERISTIC_UUID      = CBUUID(string:"2A38")
    let HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID  = CBUUID(string:"2A29")

    
    var centralManager : CBCentralManager!
    var HRMPeripheral  : CBPeripheral!
    
    // Properties for your Object controls
    @IBOutlet var heartImage: UIImageView!
    @IBOutlet var deviceInfo: UITextView!
    @IBOutlet var bpmLabel: UILabel!
    
    @IBOutlet var scanSwitch: UISwitch!
    // Properties to hold data characteristics for the peripheral device
    var connected         : String! = ""
    var bodyData          = "Body Location: "
    var manufacturer      = "Manufacturer: "
    var deviceData        : NSString!
    var heartRate         : UInt16 = 0
    
    // Properties to handle storing the BPM and heart beat
    var heartRateBPM : UILabel! = UILabel()
    var pulseTimer   : NSTimer!
    
    ///////////////////////////////////////////////////////////////////////////

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        deviceData = nil
        heartImage.addSubview(heartRateBPM)
        
        scanSwitch.setOn(false, animated: false)
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
        
        bpmLabel.text = String("0")
    }

    @IBAction func startScanning(sender: AnyObject) {
        if scanSwitch.on {
            let services = [HRM_DEVICE_INFO_SERVICE_UUID, HRM_HEART_RATE_SERVICE_UUID]
            println("Start scanning")
            centralManager.scanForPeripheralsWithServices(services, options: nil)
        }
        else {
            centralManager.stopScan()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    ////// CBCentralManagerDelegate /////////////////////////
    
    // method called whenever you have successfully connected to the BLE peripheral
    func centralManager(central: CBCentralManager!, didConnectPeripheral peripheral: CBPeripheral!) {
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        if peripheral.state == .Connected {
            println("Connected: YES")
        }
        else {
            println("Connected: NO")
        }
    }
    
    // CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
    func centralManager(central: CBCentralManager!, didDiscoverPeripheral peripheral: CBPeripheral!, advertisementData: [NSObject : AnyObject]!, RSSI: NSNumber!) {
        println("didDiscoverPeripheral")
        let localNamePre: AnyObject? = advertisementData[CBAdvertisementDataLocalNameKey]
        if let localName = localNamePre as? String {
            if (!localName.isEmpty) {
                println("Found the heart rate monitor: \(localName)")
                self.centralManager.stopScan()
                self.HRMPeripheral = peripheral
                peripheral.delegate = self;
                self.centralManager.connectPeripheral(peripheral, options: nil)
            }
        }
    }
    
    // method called whenever the device state changes.
    func centralManagerDidUpdateState(central: CBCentralManager!) { 
        // Determine the state of the peripheral
        if central.state == .PoweredOff {
            println("CoreBluetooth BLE hardware is powered off")
        }
        else if central.state == .PoweredOn {
            println("CoreBluetooth BLE hardware is powered on and ready")
        }
        else if central.state == .Unauthorized {
            println("CoreBluetooth BLE state is unauthorized")
        }
        else if central.state == .Unknown {
            println("CoreBluetooth BLE state is unknown")
        }
        else if central.state == .Unsupported {
            println("CoreBluetooth BLE hardware is unsupported on this platform")
        }
    }
    
    
    ////// CBPeripheralDelegate /////////////////////////
    
    // CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
    func peripheral(peripheral: CBPeripheral!, didDiscoverServices error: NSError!) {
      if error == nil {
        for service in peripheral.services as [CBService] {
            println("Discovered service: \(service.UUID) \(service.UUID.data)")
            peripheral.discoverCharacteristics(nil , forService: service)
        }
      }
      else {
         println("Error discovering services: \(error)")
      }
    }
    
    // Invoked when you discover the characteristics of a specified service.
    func peripheral(peripheral: CBPeripheral!, didDiscoverCharacteristicsForService service: CBService!, error: NSError!){
        if error == nil {
            if service.UUID == HRM_HEART_RATE_SERVICE_UUID {
                for aChar in service.characteristics as [CBCharacteristic] {
                    // Request heart rate notifications
                    if aChar.UUID == HRM_MEASUREMENT_CHARACTERISTIC_UUID {
                        HRMPeripheral.setNotifyValue(true, forCharacteristic: aChar)
                        //println("Found heart rate measurement characteristic")
                    }
                    // Request body sensor location
                    else if aChar.UUID == HRM_BODY_LOCATION_CHARACTERISTIC_UUID { // 3
                        HRMPeripheral.readValueForCharacteristic(aChar)
                        //println("Found body sensor location characteristic")
                    }
                }
            }
            // Retrieve Device Information Services for the Manufacturer Name
            if service.UUID == HRM_DEVICE_INFO_SERVICE_UUID  { // 4
                    for aChar in service.characteristics as [CBCharacteristic] {
                        if aChar.UUID == HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID {
                            HRMPeripheral.readValueForCharacteristic(aChar)
                            //println("Found a device manufacturer name characteristic")
                        }
                    }
            }
            // Retrieve Device Information Services for the Manufacturer Name
            if service.UUID == HRM_BATTERY_LEVEL_CHARACTERISTIC_UUID  { // 4
                for aChar in service.characteristics as [CBCharacteristic] {
                    //if aChar.UUID == HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID {
                        //HRMPeripheral.readValueForCharacteristic(aChar)
                        println("Found: \(aChar.UUID) \(aChar.UUID.data)")
                    //}
                }
            }
        }
        else {
            println("Error discovering characteristics: \(error)")
        }
    }


    // Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
    func peripheral(peripheral: CBPeripheral!, didUpdateValueForCharacteristic characteristic: CBCharacteristic!, error: NSError!) {
        // Updated value for heart rate measurement received
        if characteristic.UUID == HRM_MEASUREMENT_CHARACTERISTIC_UUID { // 1
            // Get the Heart Rate Monitor BPM
            getHeartBPMData(characteristic, error: error)
        }
        // Retrieve the characteristic value for manufacturer name received
        if characteristic.UUID == HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID {  // 2
            getManufacturerName(characteristic!)
        }
        // Retrieve the characteristic value for the body sensor location received
        else if characteristic.UUID == HRM_BODY_LOCATION_CHARACTERISTIC_UUID {  // 3
            getBodyLocation(characteristic)
        }
        
        // Add your constructed device information to your UITextView
        deviceInfo.text = NSString(format: "%@\n%@\n%@\n", connected!, self.bodyData, self.manufacturer)  // 4
    }
    
    ////// CBCharacteristic helpers /////////////////////////
    
    // Instance method to get the heart rate BPM information
    func getHeartBPMData(characteristic : CBCharacteristic! ,error: NSError!) {
        if error == nil {
            // Get the Heart Rate Monitor BPM
            var measurmentFlags:UInt8 = 0                   // 1
            characteristic.value.getBytes(&measurmentFlags, range: NSRange(location: 0,length: 1))
        
            println("Bytes \(characteristic.value.bytes) Length \(characteristic.value.length)")
            println("Value \(measurmentFlags)")
        
            var bpm:UInt16 = 0;
            
            // Retrieve the BPM value for the Heart Rate Monitor depending on stored type (UInt8 or UInt16)
            if (measurmentFlags & 0b0000_0001) == 0 {          // 2
                
                var HrmValue : UInt8 = 0;
                characteristic.value.getBytes(&HrmValue, range: NSRange(location: 1,length: 1))
                bpm = UInt16(HrmValue)
            }
            else {
                characteristic.value.getBytes(&bpm, range: NSRange(location: 1,length: 2)) // 3
            } // if measurmentFlags
            
            heartRate = bpm;
            println("bpm: \(heartRate)")
            bpmLabel.text = String("\(heartRate)")
            doHeartBeat()
            
            let tickTime: NSTimeInterval = 60 / Double(bpm)
            pulseTimer = NSTimer.scheduledTimerWithTimeInterval(tickTime, target: self, selector: Selector("doHeartBeat"), userInfo: nil, repeats: false)
        } // if error
    }
    
    // Instance method to get the manufacturer name of the device
    func getManufacturerName(characteristic : CBCharacteristic!) {
        println("+++ getManufacturerName +++")
        println("Bytes \(characteristic.value.bytes) Length \(characteristic.value.length)")
        println("--- getManufacturerName ---")
        var manufacturerName = NSString(data:characteristic.value, encoding:NSUTF8StringEncoding)  // 1
        self.manufacturer += "\(manufacturerName)"    // 2
        println(manufacturer)
    }
    
    // Instance method to get the body location of the device
    func getBodyLocation(characteristic : CBCharacteristic!) {
        var bodyLocation:UInt8 = 0                   // 1
        characteristic.value.getBytes(&bodyLocation, range: NSRange(location: 0,length: 1))
        
        //println("+++ getBodyLocation +++")
        //println("Bytes \(characteristic.value.bytes) Length \(characteristic.value.length)")
        //println("Value \(bodyLocation)")
        //println("--- getBodyLocation ---")
        
        
        switch bodyLocation { // 3
        case 0:
            bodyData += "Other"
        case 1:
            bodyData += "Chest"
        case 2:
            bodyData += "Wrist"
        case 3:
            bodyData += "BFinger"
        case 4:
            bodyData += "Hand"
        case 5:
            bodyData += "Ear Lobe"
        case 6:
            bodyData += "Foot"
        default:
            bodyData += "Reserved"
        }
        println(bodyData)
    }
    
    // Helper method to perform a heartbeat animation
    func doHeartBeat() {
//        CALayer *layer = [self heartImage].layer;
        var layer : CALayer = heartImage.layer
//        CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        var pulseAnimation = CABasicAnimation(keyPath: "transform.scale")
//        pulseAnimation.toValue = [NSNumber numberWithFloat:1.1];
        pulseAnimation.toValue = 1.1
//        pulseAnimation.fromValue = [NSNumber numberWithFloat:1.0];
        pulseAnimation.fromValue = 1.0
//        
//        pulseAnimation.duration = 60. / self.heartRate / 2.;
        pulseAnimation.duration = 60 / Double(heartRate) / 2
//        pulseAnimation.repeatCount = 1;
        pulseAnimation.repeatCount = 1
//        pulseAnimation.autoreverses = YES;
        pulseAnimation.autoreverses = true
//        pulseAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
        pulseAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
//        [layer addAnimation:pulseAnimation forKey:@"scale"];
        layer.addAnimation(pulseAnimation, forKey: "scale")
//        
//        self.pulseTimer = [NSTimer scheduledTimerWithTimeInterval:(60. / self.heartRate) target:self selector:@selector(doHeartBeat) userInfo:nil repeats:NO];
        pulseTimer = NSTimer.scheduledTimerWithTimeInterval(60 / Double(heartRate), target: self, selector: Selector("doHeartBeat"), userInfo: nil, repeats: false)
    }

}

